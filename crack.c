#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "md5.h"
#include "entry.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

int extractIndex(unsigned char *digest)
{
    //   Extract the first three bytes
    int idx = digest[0] * 65536 + digest[1] * 256 + digest[2];
  
    //   Use those three bytes as a 24-bit number
    return idx;
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file rainbow_file\n", argv[0]);
        exit(1);
    }

    // Open the text hashes file for reading
    FILE *hf = fopen(argv[1], "r");
    // Check to make sure file was successfully opened
    if (hf == NULL)
    {
        printf("Could not open %s\n", argv[1]);
        exit(1);
    }
    // Open the binary rainbow file for reading
    FILE *bf = fopen(argv[2], "rb");
    // Check to make sure if was successfully opened
    if (bf == NULL)
    {
        printf("Could not open %s\n", argv[2]);
        exit(1);
    }
    
    // For each hash (read one line at a time out of the file):
    char str[33];
    while (fscanf(hf, "%s\n", str) != EOF)
    {
        //   Convert hex string to digest (use hex2digest. See md5.h)
        unsigned char *digest = hex2digest(str);
        
        int index = extractIndex(digest);
        
        //   Seek to that location in the rainbow file
        int file_location = index * sizeof(struct entry);
        if (fseek(bf, file_location, SEEK_SET) < 0)
        {
            printf("fseek: %s\n", strerror(errno));
        }
        
        //   Read the entry found there
        //   Check to see if it's the one you want.
        //     If not, read the next one
        //     Repeat until you find the one you want.
        //   Display the hash and the plaintext
        struct entry tmp;
        int done = 0;
        do
        {
            fread(&tmp, sizeof(struct entry), 1, bf);
            char *newHash = digest2hex(tmp.hash);
            if (memcmp(str, newHash, 16) == 0)
            {
                printf("%s\t%s\n", str, tmp.pass);
                done = 1;
            }
        }while (!done);
    }
    // Close the files
    fclose(hf);
    fclose(bf);
}
